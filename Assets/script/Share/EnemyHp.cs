using System;
using UnityEngine;

namespace Assets.script
{
    /// <summary>
    /// This class contains all informations on the enemies.
    /// </summary>
    public class EnemyHp : MonoBehaviour
    {
        public int HP = 5;
        public bool IsDead = false;
        public float DyingTime = 3.0f;

        // Update is called once per frame
        void Update () 
        {
            if (IsDead)
            {
                DestroyEnemy(DyingTime);
            }	
        }

        void OnCollisionEnter( Collision collision )
        {
            if (!collision.gameObject.name.Contains("Bullet")) return;
            if (IsDead) return;
            print(string.Format("{0}: Ouch!", this.gameObject.name));

            try
            {
                // Retreive the Bullet instance.
                var playerBullet = collision.gameObject.GetComponent<Bullet>();
                // Reduce Enemy HP.
                HP -= playerBullet.BulletDamage;
                Debug.Log(string.Format("{0} -> Hp : {1}", this.gameObject.name, HP));
                if (HP <= 0)
                {
                    //DestroyEnemy();
                    IsDead = true;
                }
            }
            catch (NullReferenceException exception)
            {
                // Log exception
                Debug.Log(string.Format("No Bullet.cs script is attached on {0} \n{1}!", collision.gameObject.name, exception.Message));
            }
        }

        /// <summary>
        /// Destroy the ennemy game object.
        /// </summary>
        public void DestroyEnemy(float dyingTime)
        {
            UnFreezePhysics();
            Destroy(this.transform.parent.gameObject, dyingTime);
        }

        /// <summary>
        /// Activate the physics engine.
        /// </summary>
        public void UnFreezePhysics()
        {
            this.gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
        }
    }
}
