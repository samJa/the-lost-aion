using Assets.script;
using UnityEngine;
using System.Collections;

public class Shooter : MonoBehaviour 
{
    public Rigidbody bullet;
	public float velocity = 10f;
    public GameObject playerBullet;
    
    /// <summary>
    /// The bullet damage to modify is overrideDamage is set to true.
    /// </summary>
    public int CustomBulletDamage = 2;
    
    /// <summary>
    /// The flag that specifies if the bullet damage is customized.
    /// </summary>
    public bool OverrideDamage = false;

    void Start()
    {
        if (CustomBulletDamage > 2)
        {
            OverrideDamage = true;
        }
    }

	// Update is called once per frame
	void Update ()
	{
	    if (!Input.GetButtonUp("Fire1")) return;
	    if (OverrideDamage)
	    {
	        var bulletPrefab =
	            Instantiate(playerBullet, transform.position, playerBullet.rigidbody.rotation) as GameObject;

	        // Change the default bullet damage of the bullet prefab.
	        bulletPrefab.GetComponent<Bullet>().BulletDamage = CustomBulletDamage;
	        //bulletPrefab.GetComponent<Rigidbody>().AddForce(transform.forward * velocity, ForceMode.VelocityChange);
	        bulletPrefab.rigidbody.AddForce(transform.forward * velocity, ForceMode.VelocityChange);
	    }
	    else
	    {
	        var newBullet = Instantiate(bullet, transform.position, bullet.transform.rotation) as Rigidbody;
	        newBullet.AddForce(transform.forward * velocity, ForceMode.VelocityChange);
	    }
	}
}
