﻿using UnityEngine;

namespace Assets.script
{
    /// <summary>
    /// The SpaceBroom destroy any game objects that collides with this script.
    /// </summary>
    public class SpaceBroom : MonoBehaviour 
    {
        /// <summary>
        /// Destroy any object that collides with this rigid body.
        /// </summary>
        /// <param name="gCollision">The current rigid body that trigger the colision</param>
        void OnCollisionEnter(Collision gCollision)
        {
            Debug.Log(string.Format("{0} has destroyed : {1}", this.gameObject.name, gCollision.gameObject.name));
            //Destroy(gCollision.gameObject);
            Destroy(gCollision.transform.parent != null ? gCollision.transform.parent.gameObject : gCollision.gameObject);
        }
    }
}
