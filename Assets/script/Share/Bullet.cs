﻿using System.Diagnostics;
using UnityEngine;

namespace Assets.script
{
    public class Bullet : MonoBehaviour
    {
        public int DefaultDamage = 2;
        public int BulletLifeSpan = 2;
        public float LifeSpanAfterCollision = 3.0f;
        private Stopwatch _stopwatch;
        // public GameObject bulletType;

        // Use this for initialization
        void Awake()
        {
            BulletDamage = DefaultDamage;
        }

        void Start()
        {
            _stopwatch = new Stopwatch();
            _stopwatch.Start();
        }

        // Update is called once per frame
        void Update()
        {
            if (_stopwatch.Elapsed.Seconds > BulletLifeSpan)
            {
                _stopwatch.Stop();
                Destroy(this.gameObject);
            }
        }

        void OnCollisionEnter(Collision collision)
        {

            // code a utilisé si jamais on a besoin de changer l'attitude des projectiles selon les layers

            /*switch (collision.gameObject.layer)
            {
                // if the collider object is a default layer
                case 0:
                    Destroy(this.gameObject);
                    break;
                case 9:
                    // if the collider object is an enemy layer
                    Destroy(this.gameObject, LifeSpanAfterCollision);
                    //Destroy(this.gameObject);
                    break;
            }*/

            Destroy(this.gameObject);

            // if the collider is an asteroid
            // apply the damage, minus the asteroid resistance armor
            // then destroy the bullet game object    

            //if (collision.CompareTag("Player"))
            //    Destroy(other.gameObject);
        }

        /// <summary>
        /// BulletDamage Property.
        /// </summary>
        public int BulletDamage { get; set; }
    }
}
