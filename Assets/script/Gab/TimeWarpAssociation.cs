﻿using UnityEngine;
using System.Collections;
namespace Assets.script {
    public class TimeWarpAssociation : MonoBehaviour {

        private GameObject TimeWarpGameObject;
        private TimeWarp TimeWarpObject;
        
	    // Use this for initialization
	    void Awake () {
            TimeWarpGameObject = GameObject.Find("TimeWarpGameObject");
            TimeWarpObject = TimeWarpGameObject.GetComponent<TimeWarp>();
            TimeWarpObject.addTimeWarpList(this.gameObject);
	    }
	
	    // Update is called once per frame
        void OnDestroy() {
            TimeWarpObject.removeTimeWarpList(this.gameObject);
        }

    }
}
