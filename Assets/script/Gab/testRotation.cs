﻿using UnityEngine;
using System.Collections;

public class testRotation : MonoBehaviour {
	
	private float test = 0f;
	private float currentRotation = 0f;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		rotateInc360(2f);
	}
	
	private void rotateInc360(float increment) {
		
		currentRotation += increment;
		
		if (currentRotation >= 360f) {
			currentRotation = currentRotation-360f;
		}
		
		if (currentRotation <= -360f) {
			currentRotation = currentRotation+360f;
		}
		
		transform.localEulerAngles = new Vector3(0f, 0f, currentRotation);
		
		
	}
}
