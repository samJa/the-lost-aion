﻿using UnityEngine;
using System;

namespace Assets.script
{
    public class Sheild : MonoBehaviour {

    public float vitesseFadeOut = 0.01f;
    public float impactFade = 0.3f;
    public float SheildHP = 4f;
    public float vitesseRecharge = 0.1f; 
    public GUITexture SheildBar;
    public int tempsAvantRecharge = 4;

    private float currentFade = 0f;
    private bool sheildDown = false;
    private float width100;
    private float maxSheildHP;
    private float finalWidth;
    private float currentWidth;
    private int timerRecharge = 0;

	void Start () {
        // code pour rendre le bouclier completement invisible a l'ouverture
        renderer.material.color = renderer.material.color * new Vector4(1f, 1f, 1f, 0f);

        // code initial pour la barre de sheild
        maxSheildHP = SheildHP;
        width100 = SheildBar.pixelInset.width;
        currentWidth = width100;
        finalWidth = width100;

        // code permettant de transformer le temps de recharge en seconde selon le framerate
        tempsAvantRecharge *= 30;
	}
	
	// Update is called once per frame
	void Update () {

        //code responsable de la recharge du sheild durant les moments hors combat
        timerRecharge++;
        if (timerRecharge >= tempsAvantRecharge && SheildHP < maxSheildHP)
        {
            sheildDown = false;
            this.collider.enabled = true;

            SheildHP += vitesseRecharge;
            if (SheildHP > maxSheildHP) SheildHP = maxSheildHP;
            finalWidth = width100 * (SheildHP / maxSheildHP);
        }

        currentWidth = currentWidth - (currentWidth-finalWidth)*0.1f;
        SheildBar.pixelInset = new Rect(SheildBar.pixelInset.x, SheildBar.pixelInset.y, currentWidth, SheildBar.pixelInset.height);

        if (renderer.material.color.a > 0f) {
            renderer.material.color = new Vector4(renderer.material.color.r, renderer.material.color.g, renderer.material.color.b, renderer.material.color.a - vitesseFadeOut);
        }else{
            renderer.material.color = renderer.material.color * new Vector4(1f, 1f, 1f, 0f);
        }

    }

    void OnCollisionEnter( Collision collision )  
        {

            if (!collision.gameObject.name.Contains("BulletEnemy")) return;
            if (sheildDown) return;
    
            print(string.Format("{0}: hey!", this.gameObject.name));

            ressetTimer();

            try
            {
                // Retreive the Bullet instance.
                var ennemyBullet = collision.gameObject.GetComponent<Bullet>();
                renderer.material.color = new Vector4(renderer.material.color.r, renderer.material.color.g, renderer.material.color.b, renderer.material.color.a + impactFade);
                
                // Reduce Enemy HP.
                SheildHP -= ennemyBullet.BulletDamage;
                finalWidth = width100 * (SheildHP / maxSheildHP);

                //Debug.Log(string.Format("{0} -> Hp : {1}", this.gameObject.name, SheildHP));

                if (SheildHP <= 0)
                {
                    print("sheild down :");
                    sheildDown = true;
                    finalWidth = 0;
                    this.collider.enabled = false;
                }
            }

            catch (NullReferenceException exception)
            {
                // Log exception
                Debug.Log(string.Format("No Bullet.cs script is attached on {0} \n{1}!",
                    collision.gameObject.name, exception.Message));
            }

        }

        public void ressetTimer()
        {
            timerRecharge = 0;
        }
    }
}