﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Assets.script {
    public class TimeWarp : MonoBehaviour {


        private List<GameObject> TimeWarpList = new List<GameObject>();

	    // Use this for initialization
	    void Start () {

	    }

	    // Update is called once per frame
	    void Update () {

            /*
            for (int i=0; i<TimeWarpList.Count; i++) {
                float currentVelocityX = TimeWarpList[i].rigidbody.velocity.x * 0.5f;
                float currentVelocityY = TimeWarpList[i].rigidbody.velocity.y * 0.5f;
                float currentVelocityZ = TimeWarpList[i].rigidbody.velocity.z * 0.5f;

                Vector3 currentVelocity = new Vector3(currentVelocityX, currentVelocityY, currentVelocityZ);
                TimeWarpList[i].rigidbody.AddForce(-currentVelocity, ForceMode.VelocityChange);
	        }
            */

	    }

        public void addTimeWarpList(GameObject addObject){
            TimeWarpList.Add(addObject);

            /*Vector3 test = new Vector3(0f, 0f, 100f);
            addObject.rigidbody.AddForce(test, ForceMode.VelocityChange);

            float currentVelocityX = addObject.rigidbody.velocity.x;
            float currentVelocityY = addObject.rigidbody.velocity.y;
            float currentVelocityZ = addObject.rigidbody.velocity.z;

            Vector3 currentVelocity = new Vector3(currentVelocityX, currentVelocityY, currentVelocityZ)*-1;

            addObject.rigidbody.AddForce(currentVelocity, ForceMode.VelocityChange);
            print(currentVelocity.magnitude);*/
        }

        public void removeTimeWarpList(GameObject removeObject) {
            TimeWarpList.Remove(removeObject);
        }

        public List<GameObject> getTimeWarpList() {
            return TimeWarpList;
        }
    }
}