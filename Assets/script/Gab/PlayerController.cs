using UnityEngine;
using System.Collections;

namespace Assets.script {
    public class PlayerController : MonoBehaviour {

	    public float sensibiliteSouris = 8.5f;
	    public float vitesseRotation = 2f;
	    public float nbTourBarrelRoll = 5;
	    public int sensibiliteBarrelRoll = 10;

	    private bool rotationActiveA = false;
	    private bool rotationActiveD = false;
	    private float rotationDirection = 1f;
	    private float currentRotation = 0f;
	
	    private string mode = "normal";
	
	    private bool barrelRoll = false;
	    private int barrelRollCounterA = 0;
	    private int barrelRollCounterD = 0;
	    private int timerBarrelRoll = 0;
	    private float degBarrelRoll = 0;
	    private float degInc = 0;
	    private float distanceDeg = 0;
        private PlayerHP PlayerHPObject;
        
	    // Use this for initialization
	
        void Start () {
		    Screen.showCursor = false;
		    degBarrelRoll = 360f * nbTourBarrelRoll;
            PlayerHPObject = this.gameObject.GetComponent<PlayerHP>();
	    }
	
	    // Update is called once per frame
	    void Update () {
            if (!PlayerHPObject.getIsDead()) {

		        // modification de la direction fait par la souris
		        Vector3 direction = new Vector3(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"), 0);
		        transform.position += direction*sensibiliteSouris*Time.deltaTime;
		
		        // controle du barrel roll
		        if (barrelRollCounterA == 2 || barrelRollCounterD == 2) {
			        barrelRoll = true;
			        degBarrelRoll = degBarrelRoll - Mathf.Abs(currentRotation);
			        degInc = 0;
			        barrelRollCounterA = 0;
			        barrelRollCounterD = 0;
		        }
		
		        //Partie servant a savoir si le joueur a taper 2 fois sur la touche A ou D rapidement.
		        //Plus sensibiliteBarrelRoll est haut, plus le temps alloué est long
		        timerBarrelRoll++;
		        if (timerBarrelRoll%sensibiliteBarrelRoll == 0) {
			        barrelRollCounterA = 0;
			        barrelRollCounterD = 0;
		        }
		
		        if (barrelRoll) {
						
			        distanceDeg = (degBarrelRoll-degInc)*0.1f;
			        degInc += distanceDeg;
			
			        rotateInc360(distanceDeg*rotationDirection);
			
			        if (distanceDeg < 0.1f){
				        degInc = 0f;
				        rotateFix360(0);
				        barrelRoll = false;
				        degBarrelRoll = 360f * nbTourBarrelRoll;
			        }
			
		        }
		
		        //print (barrelRoll);
		        // control de la rotation du vaisseau pour le mettre en mode vertical
		        if (!barrelRoll) {
			        if ( Input.GetKeyDown("d") ) {
				        rotationActiveD = true;
				        rotationDirection = -1;
			        }
			
			        if ( Input.GetKeyUp("d") ) {
				        rotationActiveD = false;
				        barrelRollCounterA++;
			        }
			
			        if ( Input.GetKeyDown("a")) {
				        rotationActiveA = true;
				        rotationDirection = 1;
			        }
			
			        if ( Input.GetKeyUp("a") ) {
				        rotationActiveA = false;
				        barrelRollCounterD++;
			        }
		
			        if (rotationActiveA || rotationActiveD) {
				
				        //print("gauche:" + rotationActiveA + " : " + "droit:" + rotationActiveD);
				        float increment = rotationDirection*vitesseRotation;
				        if (currentRotation+increment>= 90f) {
					        rotateFix360(90f);
				        } else if (currentRotation+increment <= -90f){
					        rotateFix360(-90f);
				        }else{
					        rotateInc360(increment);
				        }
				
			        } else {
				
				        if (currentRotation != 0f) {
					        if (currentRotation < 0f) {
						        rotationDirection = -1;
					        }else{
						        rotationDirection = 1;
					        }
					        rotateInc360(rotationDirection*vitesseRotation*-1);
				        }
				
				        //rotation vers la direction que le vaisseau prend
				        Vector3 directionRotation = direction;
				        directionRotation.z = -0.1f;
				
				        //transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(directionRotation*-1), Mathf.Deg2Rad*2.0f);
			        }
		        }
            }
	    }
	
	    private void rotateInc360(float increment) {
		
		    currentRotation += increment;
		
		    if (currentRotation >= 360f) {
			    currentRotation = currentRotation-360f;
		    }
		
		    if (currentRotation <= -360f) {
			    currentRotation = currentRotation+360f;
		    }
		
		    transform.localEulerAngles = new Vector3(0f, 0f, currentRotation);
		
	    }
	
	    private void rotateFix360(float degValue) {
		    transform.localEulerAngles = new Vector3(0f, 0f, degValue);
		    currentRotation = degValue;
	    }

        public bool GetBarrellRollStatus()
        {
            return barrelRoll;
        }
    }
}