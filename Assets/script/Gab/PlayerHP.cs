﻿
using UnityEngine;
using System;

namespace Assets.script
{
    public class PlayerHP : MonoBehaviour
    {
        public int HP = 5;
        public float DyingTime = 3.0f;
        public GUITexture HPBar;

        private bool IsDead = false;
        private float width100;
        private float maxHP;
        private float finalWidth;
        private float currentWidth;

        // Update is called once per frame
        private Sheild SheildObject;

        public int HitPoints 
        { get { return HP; }
          set
          {
              HP = value;
              finalWidth = width100 * (HP / maxHP);

              //Debug.Log(string.Format("{0} -> Hp : {1}", this.gameObject.name, HP));

              if (HP <= 0)
              {
                  DestroyPlayer(DyingTime);
                  IsDead = true;
                  finalWidth = 0;
              }
          } 
        }

        void Start()
        {
            HitPoints = HP;
            maxHP = HitPoints;
            width100 = HPBar.pixelInset.width;
            currentWidth = width100;
            finalWidth = width100;

            SheildObject = this.gameObject.GetComponentInChildren<Sheild>();
        }

        // Update is called once per frame
        void Update()
        {
            currentWidth = currentWidth - (currentWidth - finalWidth) * 0.1f;
            HPBar.pixelInset = new Rect(HPBar.pixelInset.x, HPBar.pixelInset.y, currentWidth, HPBar.pixelInset.height);

            if (IsDead) {
                DestroyPlayer(DyingTime);
            }
        }

        void OnCollisionEnter(Collision collision)
        {
            if (!collision.gameObject.name.Contains("Bullet")) return;
            if (IsDead) return;

            SheildObject.ressetTimer();
            //print(string.Format("{0}: Ouch!", this.gameObject.name));

            try
            {
                // Retreive the Bullet instance.
                var playerBullet = collision.gameObject.GetComponent<Bullet>();

                // Reduce Enemy HP.
                HitPoints -= playerBullet.BulletDamage;
                //finalWidth = width100 * (HP / maxHP);

                ////Debug.Log(string.Format("{0} -> Hp : {1}", this.gameObject.name, HP));
                //if (HP <= 0)
                //{
                //    DestroyPlayer(DyingTime);
                //    IsDead = true;
                //    finalWidth = 0;
                //}
            }

            catch (NullReferenceException exception)
            {
                // Log exception
                Debug.Log(string.Format("No Bullet.cs script is attached on {0} \n{1}!",
                    collision.gameObject.name, exception.Message));
            }
        }

        /// <summary>
        /// Width property.
        /// </summary>
        public float Width
        {
            get { return width100; }
            set { width100 = value; }
        }

        /// <summary>
        /// MaxHP property.
        /// </summary>
        public float MaxHP
        {
            get { return maxHP; }
            set { maxHP = value; }
        }

        /// <summary>
        /// FinalWitdth property.
        /// </summary>
        public float FinalWidth
        {
            get { return finalWidth; }
            set { finalWidth = value; }
        }

        /// <summary>
        /// getIsDead, accessor.
        /// </summary>
        /// <returns>true if the player is dead</returns>
        public Boolean getIsDead() {
            return IsDead;
        }

        /// <summary>
        /// Destroy the player game object.
        /// </summary>
        /// <param name="dyingTime">The time before the game object is destroyed</param>
        public void DestroyPlayer(float dyingTime)
        {
            this.gameObject.rigidbody.constraints = RigidbodyConstraints.None;
            Destroy(this.transform.parent.gameObject, dyingTime);
        }
    }
}
