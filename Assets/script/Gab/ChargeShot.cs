﻿using UnityEngine;
using System.Collections;

namespace Assets.script {
    public class ChargeShot : MonoBehaviour {

        public ParticleSystem chargeShot;
        public float chargeShotEmission = 80.64f;
        public float energyCost = 0f;

        public GameObject collisionSphere;
        public Transform playerPos;
        public Transform launchPoint;
        public float force = 100;
        public float collisionLifeTime;

        public float startfire = 0f;
        public float interval = 0f;

        private PlayerHP PlayerHPObject;
        private PlayerEnergy PlayerEnergyObject;
        private bool emmiterOn = false;

	    // Use this for initialization
	    void Start () {
            PlayerHPObject = this.gameObject.GetComponent<PlayerHP>();
            PlayerEnergyObject = this.gameObject.GetComponent<PlayerEnergy>();
            chargeShot.emissionRate = 0f;
	    }
	
	    // Update is called once per frame
	    void Update () {
            if (!PlayerHPObject.getIsDead()) {

                if (Input.GetKeyDown("q") && !emmiterOn)
                {
                    chargeShot.emissionRate = chargeShotEmission;
                    InvokeRepeating("CreateCollider", startfire, interval);
                    emmiterOn = true;
                    PlayerEnergyObject.setUseEnergy(energyCost);
                }

                if (Input.GetKeyUp("q") && emmiterOn || PlayerEnergyObject.getOutOfEnergy())
                {
                    chargeShot.emissionRate = 0f;
                    CancelInvoke("CreateCollider");
                    emmiterOn = false;
                    PlayerEnergyObject.setUseEnergy(0f);
                }

            }

	    }

        void CreateCollider()
        {
            var b = (GameObject)Instantiate(collisionSphere);
            b.transform.position = launchPoint.position;

            Vector3 direction = playerPos.position - transform.position;
            Quaternion rotation = Quaternion.LookRotation(direction);
            b.transform.rotation = rotation;

            b.rigidbody.AddForce(b.transform.forward * force, ForceMode.VelocityChange);
            Destroy(b, collisionLifeTime);
        }
    }
}