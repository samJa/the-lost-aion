﻿using UnityEngine;
using System.Collections;

namespace Assets.script
{
    public class PlayerEnergy : MonoBehaviour {
        public float energy = 20;
        public GUITexture EnergyBar;

        private float width100;
        private float maxEnergy;
        private float useEnergy = 0f;
        private bool outOfEnergy = false;

	    // Use this for initialization
	    void Start () {
            maxEnergy = energy;
            width100 = EnergyBar.pixelInset.width;
	    }
	
	    // Update is called once per frame
	    void Update () {
            if (useEnergy == 0f && energy != 0f) return;
            
            if (energy - useEnergy > 0f) {
                energy -= useEnergy;
            } else {
                energy = 0f;
                useEnergy = 0f;
                outOfEnergy = true;
            }

            EnergyBar.pixelInset = new Rect(EnergyBar.pixelInset.x, EnergyBar.pixelInset.y, width100 * (energy / maxEnergy), EnergyBar.pixelInset.height);
	    }

        public void setUseEnergy(float newUseEnergy) {
            useEnergy = newUseEnergy;
        }

        public float getEnergy()
        {
            return energy;
        }
        public bool getOutOfEnergy()
        {
            return outOfEnergy;
        }
    }
}