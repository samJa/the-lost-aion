using UnityEngine;
using System.Collections;

public class sceneController : MonoBehaviour {
	
	public float speed = 1.0f;
	public float speedAcceleration = 1.0f;
	public float speedBreak = 1.0f;
	private float actualSpeed;
	
	void Start() {
		actualSpeed = speed;
	}
	
	// Update is called once per frame
	
	void Update () {
		
		transform.position += transform.forward*actualSpeed*Time.deltaTime;
		
		if ( Input.GetKeyDown("w") ) {
			actualSpeed = speed + speedAcceleration;
		}
		
		if ( Input.GetKeyUp ("w") ) {
			actualSpeed = speed;
		}
		
		if ( Input.GetKeyDown("s") ) {
			actualSpeed = speed - speedBreak;
		}
		
		if ( Input.GetKeyUp ("s") ) {
			actualSpeed = speed;
		}
		
	}
}
