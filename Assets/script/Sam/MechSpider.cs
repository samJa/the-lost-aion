﻿using System.Diagnostics;
using UnityEngine;
using Debug = UnityEngine.Debug;

namespace Assets.script
{
    public class MechSpider : MonoBehaviour
    {
        /// <summary>
        /// The player game object to with this current game object will stick to.
        /// </summary>
        public  Transform TargetToDamage;

        /// <summary>
        /// The count down before the damage calculations occurs.
        /// </summary>
        public int ExplosionCountdown;

        /// <summary>
        /// The amount of hit points the target will take.
        /// </summary>
        public int ExplosionDamage;

        public float DyingTime = 3.0f;

        private Stopwatch _stopwatch;

        // Use this for initialization
        void Start () 
        {
	        _stopwatch = new Stopwatch();
        }
	
        // Update is called once per frame
        void Update () 
        {
            if (IsGlued)
            {
                // play the countdown animaiton or start the stop watch
                _stopwatch.Start();
                if (_stopwatch.Elapsed.Seconds > ExplosionCountdown)
                {
                    _stopwatch.Stop();
                    DamagePlayer();
                }
            }
        }

        /// <summary>
        /// Reduce the players hp.
        /// </summary>
        private void DamagePlayer()
        {
           
            // start stopwatch or for loop
            //_stopwatch.Start();

            //do
            //{
                // loop an explosion countdown animation or a Thread.sleep(ExplosionCoundown)
              
                // make the spider blink or gradually turn red.
            //} while (_stopwatch.Elapsed.Seconds < ExplosionCountdown);

            //_stopwatch.Stop();

            // reduce the player hit points
           
            var playerHP = TargetToDamage.gameObject.GetComponent<PlayerHP>();
            //Debug.Log("Player HP : " + playerHP.HP);
            // reduce player hit points.
           
            playerHP.HitPoints -= ExplosionDamage;
            //playerHP.FinalWidth = playerHP.Width * (playerHP.HP / playerHP.MaxHP);
            //Debug.Log(string.Format("{0} : {1}", TargetToDamage.gameObject.name, playerHP.HP.ToString()));
            // The spider is still glued to the player but we set this value to false. To prevent this function to be called
            // every frame.
            IsGlued = false;
            ExplodeSpider();
        }

        /// <summary>
        /// Destroy the parent and the current game object.
        /// </summary>
        private void DestroySpider()
        {  
            // Destroy the spider game object
            Destroy(this.transform.parent.gameObject, DyingTime);
        }

        /// <summary>
        /// Play an animation then destroy the parent and the current game object.
        /// </summary>
        private void ExplodeSpider()
        {
            // Play an explosion animation
            // or play a fire particule, etc
            
            // destroy the enemy 
            Destroy(this.transform.parent.gameObject);
        }

        /// <summary>
        /// Triggers when the game object collides with the target.
        /// </summary>
        /// <param name="gCollision">The game object witch have a rigidbody attached.</param>
        public void OnCollisionEnter(Collision gCollision)
        {
            // if the game object collides with a rigidbody that have a layer value set to "player".
            if (gCollision.gameObject.layer == 8)
            {
                var isBarrelRoll = TargetToDamage.gameObject.GetComponent<PlayerController>().GetBarrellRollStatus();
                //if (Target.gameObject == gCollision.gameObject)
                //{
                if (!isBarrelRoll)
                {
                    var colliderName = this.transform.name;
                    var colliderGameObjectName = gCollision.transform.gameObject.name;
                    //Debug.Log(string.Format("ColliderName: {0}", colliderName));
                    //Debug.Log(string.Format("ColliderGameObjectName : {0}", colliderGameObjectName));

                    var currentObjectParent = this.transform.parent;
                    var currentParentGameObject = this.transform.parent.gameObject;
                    //Debug.Log(string.Format("CurrentObjectParent : {0}", currentObjectParent));
                    //Debug.Log(string.Format("CurrentParentGameObject : {0}", currentParentGameObject));

                    this.transform.parent.parent = gCollision.transform;
                    FreezePhysics();
                    IsGlued = true;
                    //TargetToDamage = gCollision.transform;
                    //DamagePlayer(gCollision);
                }
                else
                {
                    UnFreezePhysics();
                    DestroySpider();
                }
            }
        }

        /// <summary>
        /// This function, make the current game object not affected
        /// by the actual physics engine. This function is called to 
        /// prevent the game objects from falling due to the reactions of collisions with objets.
        /// </summary>
        public void FreezePhysics()
        {
            this.gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
        }

        /// <summary>
        /// Activate the physics engine.
        /// </summary>
        public void UnFreezePhysics()
        {
            this.gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
        }

        /// <summary>
        /// Property that tell the other class if the current game object
        /// is glued to the player.
        /// </summary>
        public bool IsGlued { get; private set; }
    }
}
