using UnityEngine;
using System.Collections;

public class EnemyHp : MonoBehaviour
{
   public int HP = 5;
   public bool IsDead = false;
	// Use this for initialization
	void Start () 
   {
	
	}
	
	// Update is called once per frame
	void Update () 
   {
	   if (IsDead)
	   {
	      DestroyEnemy();
      }	
	}

   void OnCollisionEnter( Collision collision )
   {
   }

   public void DestroyEnemy()
   {
      Destroy( this.gameObject );
   }
}
