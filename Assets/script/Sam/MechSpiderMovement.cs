﻿using UnityEngine;

namespace Assets.script.Sam
{
    /// <summary>
    /// This class allows the movement of the Mech spiders. 
    /// </summary>
    public class MechSpiderMovement : MonoBehaviour
    {
        /// <summary>
        /// The target to with this current game object will move.
        /// </summary>
        public Transform Target;

        /// <summary>
        /// The speed movement.
        /// </summary>
        public int Speed;

        /// <summary>
        /// Specifies if the target is reached.
        /// </summary>
        private bool _targetReached;

        /// <summary>
        /// Specifies if the current game object is moving.
        /// </summary>
        private bool _isMoving;

        /// <summary>
        /// The direction to where the current game object is moving.
        /// </summary>
        private Vector3 _direction;

        // Use this for initialization
        void Start ()
        {
            _isMoving = false;
            _targetReached = false;
        }
	
        // Update is called once per frame
        void Update () 
        {
            // if the current game object is still moving and did not reach the target.
            if( _isMoving && !_targetReached )
            {
                MoveObject(Speed);
            }
        }

        /// <summary>
        /// Move the current game object towards the position of the target game object.
        /// </summary>
        /// <param name="speed">the speed at with the game object moves.</param>
        public void MoveObject(int speed)
        {
            if (_targetReached) return;
            // rotates the game object and translate towards the target.
            // Do this after we have the assets for the spiders.

            // Move the object until the mech spider reach the player.
            _direction = Target.position - transform.position;

            // source : http://answers.unity3d.com/questions/222859/make-a-game-object-move-towards-another-object-and.html
            transform.Translate(_direction*speed*Time.deltaTime, Space.World);

            if (transform.position != Target.position) return;
            _targetReached = true;
            _isMoving = false;
        }

        /// <summary>
        /// Trigger when the player enter the field of view of the Mech spider.
        /// </summary>
        /// <param name="gCollider">The game object that have a player layer.</param>
        public void OnTriggerEnter(Collider gCollider)
        {
            // if the game object collides with a rigidbody that have a layer value set to "player".
            if (gCollider.gameObject.layer != 8) return;
            Debug.Log(string.Format("{0} breach the field of view of : {1}", gCollider.gameObject.name, this.gameObject.name));
            _isMoving = true;
        }
    }
}
