using Assets.script;
using UnityEngine;

namespace Assets.script
{
    public class RotationPivot : MonoBehaviour
    {
        /// <summary>
        /// The target where the object will rotate towards.
        /// </summary>
        public Transform Target;

        /// <summary>
        /// The current object that will perform the rotation.
        /// </summary>
        public Transform ObjectToMove;

        /// <summary>
        /// The rotation speed.
        /// </summary>
        public float Speed;

       /// <summary>
       /// Activated when the current object is in rotation mode.
       /// </summary>
        private bool _rotationEngaged;

        /// <summary>
        /// The default position of the current object that will rotate.
        /// </summary>
        private Vector3 _objectToMoveDefaultPosition;

        /// <summary>
        /// This method is called at the start of the application.
        /// </summary>
        void Start()
        {
            _rotationEngaged = false;
            _objectToMoveDefaultPosition = ObjectToMove.position;
        }

        /// <summary>
        /// This method is called every frame.
        /// </summary>
        private void Update()
        {
            if (!_rotationEngaged) return;
            var targetDir = Target.position - ObjectToMove.position;
            var step = Speed*Time.deltaTime;
            var direction = ObjectToMove.forward;
            ObjectToMove.RotateObject(direction, targetDir, step);
        }

        /// <summary>
        /// Called every time a rigid body or a trigger collides with the current GameObject.
        /// </summary>
        /// <param name="otherObject">See <see cref="Collider"/></param>
        void OnTriggerEnter(Collider otherObject)
        {
            // If the current object that trigger the collision is the actual target
            if (otherObject.gameObject == Target.gameObject)
            {
                _rotationEngaged = true;
            }
        }

        /// <summary>
        /// Called when a rigidbody object exit the field of view.
        /// </summary>
        void OnTriggerExit(Collider otherObject)
        {       
            if (otherObject.gameObject != Target.gameObject) return;
            var directionToRotate = _objectToMoveDefaultPosition - ObjectToMove.position;
            var step = Speed * Time.deltaTime;
            ObjectToMove.RotateObject(Vector3.forward, directionToRotate, step);
            _rotationEngaged = false;
        }
    }
}
