﻿using System.Diagnostics;
using Assets.script;
using UnityEngine;
using System.Collections;
using Debug = UnityEngine.Debug;

public class SpaceMine : MonoBehaviour
{
    public int ExplosionDamage;
    public int ExplosionRadius;
    public int ExplosionCountdown;
    public Transform Target;

    //private Stopwatch _stopwatch;
	
    // Use this for initialization
	void Start () 
    {
        //_stopwatch = new Stopwatch();
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}

    void OnTriggerEnter(Collider gCollider)
    {
        // if the game object collides with a rigidbody that have a layer value set to "player".
        if (gCollider.gameObject.layer == 8)
        {
            DamagePlayer();
        }
    }

    /// <summary>
    /// Reduce the players hp.
    /// </summary>
    private void DamagePlayer()
    {

        // start stopwatch or for loop
        //_stopwatch.Start();

        //do
        //{
        // loop an explosion countdown animation or a Thread.sleep(ExplosionCoundown)

        // make the spider blink or gradually turn red.
        //} while (_stopwatch.Elapsed.Seconds < ExplosionCountdown);

        //_stopwatch.Stop();

        // retreive the player hit points
        var playerHP = Target.gameObject.GetComponent<PlayerHP>();
        Debug.Log("Player HP before explosion: " + playerHP.HP);
        // reduce player hit points.
        playerHP.HitPoints -= ExplosionDamage;
        //playerHP.FinalWidth = playerHP.Width * (playerHP.HP / playerHP.MaxHP);
        //Debug.Log("Player HP after explosion: " + playerHP.HP);
        //Debug.Log(string.Format("{0} : {1}", TargetToDamage.gameObject.name, playerHP.HP.ToString()));
        ExplodeSpaceMine();
    }

    /// <summary>
    /// Play an animation then destroy the parent and the current game object.
    /// </summary>
    private void ExplodeSpaceMine()
    {
        // Play an explosion animation
        // or play a fire particule, etc
        // destroy the enemy 
        Destroy(this.gameObject);
    }
}
