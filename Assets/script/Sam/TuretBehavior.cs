using UnityEngine;

namespace Assets.script
{
    public class TuretBehavior : MonoBehaviour 
    {	
	    bool isInTrigger = false;
	    public Transform playerPos;
	    public Transform launchPoint;
	    public GameObject BulletPre;
	    public float force = 100;
        public float startfire;
        public float interval;
        public float bulletLifeTime;

        private EnemyHp enemyHpObject;

        // Use this for initialization
	    void Start () 
        {
            enemyHpObject = this.gameObject.GetComponentInChildren<EnemyHp>();
	    }
	
	    // Update is called once per frame
	    void Update () 
        {
            if (isInTrigger && !enemyHpObject.IsDead) 
            {	
			    Vector3 direction = transform.position - playerPos.position;
			    Quaternion rotation = Quaternion.LookRotation(direction);
			    transform.rotation = rotation;
		    }
	    }
	
	    void OnTriggerEnter(Collider col) 
	    {
		    if (col.gameObject == playerPos.gameObject) 
            {
			    isInTrigger = true;
			    InvokeRepeating ("CreateBullet",startfire, interval);
		    }
	    }
	
	    void OnTriggerExit(Collider col) 
	    {
		    if (col.gameObject == playerPos.gameObject) 
            {
			    isInTrigger = false;
			    CancelInvoke ("CreateBullet");
		    }
	    }
	
	    void CreateBullet() 
        {
		    if (isInTrigger) 
            {
		        var b = (GameObject) Instantiate(BulletPre);
			    b.transform.position = launchPoint.position;
			
			    Vector3 direction = playerPos.position - transform.position;
			    Quaternion rotation = Quaternion.LookRotation(direction);
			    b.transform.rotation = rotation;
			
			    b.rigidbody.AddForce (b.transform.forward * force, ForceMode.VelocityChange);
			    Destroy (b, bulletLifeTime);
		    }
	    }
    }
}
