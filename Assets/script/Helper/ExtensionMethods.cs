using UnityEngine;

namespace Assets.script
{
    /// <summary>
    /// Helper class, that hold most used methods in Unity
    /// </summary>
    public static class ExtensionMethods
    {
        /// <summary>
        /// Rotate an object towards the location.
        /// </summary>
        /// <param name="objectToRotate">The current object to rotate, see <see cref="Transform"/></param>
        /// <param name="direction">The direction, see <see cref="Vector3"/></param>
        /// <param name="position">The position to rotate towards, see <see cref="Vector3"/></param>
        /// <param name="step">The step per rotation</param>
        /// <param name="magnitude"></param>
        public static void RotateObject(this Transform objectToRotate, Vector3 direction, Vector3 position, float step, float magnitude = 0.0F)
        {
            var newDir = Vector3.RotateTowards(direction, position, step, magnitude);
            Debug.DrawRay(objectToRotate.position, newDir, Color.red);
            objectToRotate.rotation = Quaternion.LookRotation(newDir);
        }
    }
}

